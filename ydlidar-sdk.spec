%global debug_package %{nil}

Name:           ydlidar-sdk
Version:        1.1.3
Release:        1
Summary:        Driver for receiving YD LiDAR data
License:        MIT
URL:            https://github.com/YDLIDAR/YDLidar-SDK
Source0:        V%{version}.tar.gz

Patch0:         ydlidar-sdk-fix-libdir.patch

BuildRequires:  gcc
BuildRequires:  swig
BuildRequires:  gtest-devel
BuildRequires:  gmock-devel
BuildRequires:  python3-devel

%description
Driver for receiving YD LiDAR data.

%package        devel
Summary:        Development files for ydlidar-sdk
Requires:       %{name} = %{version}-%{release}

%description    devel
The devel package contains development files for ydlidar-sdk.
It provides header files and libraries for ydlidar-sdk.

%prep
%setup -n YDLidar-SDK-%{version}
%patch0 -p1

%build
mkdir build && cd build
cmake \
	-DBUILD_SHARED_LIBS=ON \
	-DCMAKE_INSTALL_PREFIX="/usr" \
	-DCMAKE_INSTALL_LIBDIR="%{_libdir}" \
	..
make

%install
%make_install -C build

%files
%{_bindir}/*
%{_libdir}/*.so
%{_libdir}/python%{python3_version}/*
/usr/startup/*

%files devel
%{_includedir}/*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/cmake/*
%{_datadir}/*

%changelog
* Wed May 3 2023 will_niutao <niutao2@huawei.com> - 1.1.3-1
- Init for openEuler
